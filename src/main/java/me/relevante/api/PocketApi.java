package me.relevante.api;

import me.relevante.model.PocketItem;
import me.relevante.model.PocketProfile;
import me.relevante.model.PocketSearchTerms;
import me.relevante.network.Pocket;

import java.util.List;

public interface PocketApi extends NetworkApi<Pocket, PocketProfile> {

    List<PocketItem> getItems() throws PocketApiException;
    List<PocketItem> getItems(PocketSearchTerms searchTerms) throws PocketApiException;
    List<String> getTags(PocketSearchTerms searchTerms) throws PocketApiException;
    List<PocketItem> getItemsByTag(String tag) throws PocketApiException;

}
