package me.relevante.model;

import me.relevante.network.Pocket;

/**
 * Created by daniel-ibanez on 2/08/16.
 */
public class PocketProfile implements NetworkProfile<Pocket, PocketProfile> {

    private String id;
    private String username;

    public PocketProfile(String id) {
        this.id = id;
        this.username = id;
    }

    @Override
    public Pocket getNetwork() {
        return Pocket.getInstance();
    }

    @Override
    public String getNlpAnalyzableContent() {
        return "";
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getUserId() {
        return getId();
    }

    @Override
    public void updateDataFrom(PocketProfile pocketProfile) {
        this.id = pocketProfile.getId();
    }

    @Override
    public void completeDataFrom(PocketProfile pocketProfile) {
        this.id = (id == null) ? pocketProfile.getId() : id;
    }

    public String getUsername() {
        return username;
    }
}
