package me.relevante.model;

import me.relevante.network.Pocket;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by daniel-ibanez on 2/08/16.
 */
@Document(collection = "PocketFullUser")
public class PocketFullUser implements NetworkFullUser<Pocket, PocketProfile>, Updatable<PocketFullUser> {

    @Id
    private String id;
    private PocketProfile profile;
    private List<String> postIds;

    public PocketFullUser() {
        super();
        this.postIds = new ArrayList<>();
    }

    public PocketFullUser(PocketProfile profile) {
        this();
        this.id = profile.getId();
        this.profile = profile;
    }

    @Override
    public Pocket getNetwork() {
        return Pocket.getInstance();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public PocketProfile getProfile() {
        return profile;
    }

    @Override
    public List<String> getRelatedTerms() {
        return Arrays.asList();
    }

    @Override
    public String getUserId() {
        return profile.getId();
    }

    @Override
    public void updateDataFrom(PocketFullUser fullUser) {
        this.profile.updateDataFrom(fullUser.getProfile());
        this.id = fullUser.getId();
        this.postIds.clear();
        this.postIds.addAll(fullUser.getPostIds());
    }

    @Override
    public void completeDataFrom(PocketFullUser fullUser) {
        this.profile.completeDataFrom(fullUser.getProfile());
        this.id = (id == null) ? fullUser.getId() : id;
        completeWithMissingElements(this.postIds, fullUser.getPostIds());
    }

    public List<String> getPostIds() {
        return postIds;
    }

    private void completeWithMissingElements(List listToBeCompleted,
                                             List listToCompleteWith) {
        for (Object object : listToCompleteWith) {
            if (!listToBeCompleted.contains(object)) {
                listToBeCompleted.add(object);
            }
        }
    }
}
