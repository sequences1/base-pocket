package me.relevante.model;

/**
 * Created by daniel-ibanez on 3/08/16.
 */
public enum PocketContentType {

    ARTICLE("article"),
    VIDEO("video"),
    IMAGE("image");

    private String name;

    PocketContentType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
